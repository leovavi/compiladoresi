#ifndef _SINTAX_H
#define _SINTAX_H

#include "lexer.h"
#include "error.h"

class Sintax{
    public:
        Sintax(Lexer &lex) : lexer(lex) {};
        void parse();
    private:
        Lexer & lexer;
        Token tok;

        void E();
        void Ep();
        void T();
        void Tp();
        void F();

};

#endif /* _SINTAX_H */