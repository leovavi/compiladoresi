#include "lexer.h"

Token Lexer::getNextToken()
{
    lexema = "";
    while(ch == ' ' || ch == '\t' || ch == '\n') getNextChar();
    if(ch == EOF) {
        cout << "EOF reached" << endl;
        return Token::Eof;
    }
    while(ch == '/'){
        getNextChar();
        if(ch == '*'){
            getNextChar();
            string eoc = "";
            while(eoc.compare("*/") != 0 && ch != EOF){
                if(ch == '*' && eoc.compare("") == 0){
                    eoc += ch;
                    getNextChar();
                }else if(ch == '/' && eoc.compare("*") == 0){
                    eoc += ch;
                    getNextChar();
                }else{
                    eoc = "";
                    getNextChar();
                }
            }
            if(ch == EOF) return Token::Eof;
        }
        else if(ch == '/'){
            getNextChar();
            while(ch != '\n') getNextChar();
        }
        else{
            lexema = "/";
            return Token::Undefined;
        } 
        while(ch == ' ' || ch == '\n' || ch == '\t') getNextChar();
    }

    switch(ch){
        case '+':
            addToLexema();
            return Token::Plus;
        
        case '*':
            addToLexema();
            return Token::Mult;
        
        case '(':
            addToLexema();
            return Token::OpenPar;
        
        case ')':
            addToLexema();
            return Token::ClosePar;

        default:
            if(isdigit(ch)){
                addToLexema();
                while(isdigit(ch))
                    addToLexema();
                return Token::Num;
            }
            else{
                addToLexema();
                return Token::Undefined;
            } 
    }
}

string Lexer::TokenToString(Token tok)
{
    switch(tok){
        case Token::Num: return "Number";
        case Token::Plus: return "Plus Sign";
        case Token::Mult: return "Multiplier Sign";
        case Token::OpenPar: return "Open Parenthesis";
        case Token::ClosePar: return "Close Parenthesis";
        case Token::Eof: "Eof";
        default: return "Undefined";
    }
}

void Lexer::addToLexema()
{
    lexema += ch;
    getNextChar();
}