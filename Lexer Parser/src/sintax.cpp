#include "sintax.h"

void Sintax::parse()
{
    tok = lexer.getNextToken();
    E();
    if(tok == Token::Eof)
        cout << "Parsed Successfully!" << endl;
    else
        throw Error("Unexpected end of Parser, received: "+lexer.getLexema());
}

void Sintax::E()
{
    T();
    Ep();
}

void Sintax::Ep()
{
    if(tok == Token::Plus){
        tok = lexer.getNextToken();
        T();
        Ep();
    }
}

void Sintax::T()
{
    F();
    Tp();
}

void Sintax::Tp()
{
    if(tok == Token::Mult){
        tok = lexer.getNextToken();
        F();
        Tp();
    }
}

void Sintax::F()
{
    if(tok == Token::OpenPar){
        tok = lexer.getNextToken();
        E();
        if(tok == Token::ClosePar)
            tok = lexer.getNextToken();
        else
            throw Error("Expected ) but received: "+lexer.getLexema());    
    }
    else if(tok == Token::Num)
        tok = lexer.getNextToken();
    else 
        throw Error("Expected ( or Number but received: "+lexer.getLexema());
}