#include "expr_ast.h"

string BinaryExpr::toString(){
    string expr1Str = "("+expr1->toString()+")";
    string expr2Str = "("+expr2->toString()+")";

    return expr1Str+getOper()+expr2Str;
}