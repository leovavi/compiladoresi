#ifndef _AST_H
#define _AST_H

#include <memory>
#include <list>
#include <string>
#include <iostream>

using namespace std;

#define DEFINE_BINARY_EXPR(name, operStr, prec) \
        class name##Expr : public BinaryExpr { \
            public: \
                name##Expr(ASTNode * expr1, ASTNode * expr2) : BinaryExpr(move(expr1), move(expr2)) {}; \
                ~name##Expr(){}; \
                string getOper() override { return operStr; }; \
                int getPrec() override { return prec; }; \
        }; \

class ASTNode{
    public:
        ASTNode(){};
        virtual ~ASTNode(){};

        virtual string toString() = 0;
        virtual int getPrec() = 0;
};

class BinaryExpr : public ASTNode{
    public:
        BinaryExpr(ASTNode * expr1, ASTNode * expr2) : expr1(move(expr1)), expr2(move(expr2)){};
        ~BinaryExpr(){};

        string toString();
        virtual string getOper() = 0;

        ASTNode *expr1, *expr2;
};

DEFINE_BINARY_EXPR(Add, "+", 0);
DEFINE_BINARY_EXPR(Sub, "-", 0);
DEFINE_BINARY_EXPR(Mul, "*", 1);
DEFINE_BINARY_EXPR(Div, "/", 1);
DEFINE_BINARY_EXPR(Mod, "%", 1);

class NumExpr : public ASTNode{
    public:
        NumExpr(int value) : value(value) {};
        ~NumExpr(){};
        string toString() { return to_string(value); };
        int getPrec() override { return 10; };
        
        int value;
};

class IdExpr : public ASTNode{
    public:
        IdExpr(string name) : name(name) {};
        ~IdExpr(){};
        string toString() override { return name; };
        int getPrec() override { return 10; };

        string name;
};

#endif
