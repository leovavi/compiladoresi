#include "LR0.h"

void LR0::computeDFA() {    
    const Production * firstProd = &(gr.getFirstProduction());

    /*string_vector_t initialRhs;
    initialRhs.push_back(firstProd.lhs);
    initialRhs.push_back("$");

    const string initialSym = firstProd.lhs+"'";
    Production initialProd(initialSym, move(initialRhs));*/

    State firstState;

    firstState.items.emplace_back(*firstProd, 0);
    genDFARecursive(firstState);
    printDFA();
}

void LR0::genGraphviz(){
    struct stat sb;
    if(!(stat("../graphs", &sb) == 0 && S_ISDIR(sb.st_mode)))
        system("mkdir ../graphs");
    ofstream file;
    stringstream ss;
    file.open("../graphs/grammar.txt");
    file << "digraph{\n";

    for(int i = 0; i<dfa.size(); i++){
        State state = dfa[i];
        ss.str("");
        ss << i;
        file << "\tq" << ss.str() << " [label=\"";
        for(auto item : state.items) 
            file << item.toString() << "\n";
        file << "\"];\n";
    }

    for(int i = 0; i<dfa.size(); i++){
        State state = dfa[i];
        ss.str("");
        ss << i;
        for(auto trans : state.transitions){
            file << "\tq" << ss.str() << " -> q";
            int state = findExistingState(trans.next_state.items[0].toString());
            ss.str("");
            ss << state;
            file << ss.str() << "[label=\"" << trans.symbol << "\"];\n";
        }
    }

    file << "}";
    file.close();
}

void LR0::printDFA(){
    cout << "\nSize of dfa: " << dfa.size() << endl;
    for(int i = 0; i<dfa.size(); i++){
        cout << "\nq" << i << "------------------" << endl;
        for(auto temp : dfa[i].items)
            cout << temp.toString() << endl;
        cout << endl << "Transitions\n\n";

        for(auto temp: dfa[i].transitions)
            for(auto temp2: temp.next_state.items)
                cout << "Symbol: " << temp.symbol << " - " << temp2.toString() << endl;
    }
}

void LR0::genDFARecursive(State & st){
    cout << "Starting Recursive Function" << endl;
    for(int i = 0; i<st.items.size(); i++){
        Item & it = st.items[i];
        cout << it.dot_pos << endl;
        if(it.dot_pos == it.prod.rhs.size()){
            cout << "Skipping because . was found at the end" << endl;
            continue;
        }
        if(!isNonTerminal(it.prod.rhs[it.dot_pos])){
            cout << "Skipping because next symbol is a terminal" << endl;
            continue;
        }
        
        cout << "Getting Items" << endl;
        vector<Item> items = getItems(st, it.prod.rhs[it.dot_pos], true);
        cout << "Printing the gotten items" << endl;
        for(auto item : items)
            cout << item.toString() << endl;
        if(alreadyInItems(st.items, items[0])) continue;

        for(auto newItem : items)
            st.items.push_back(newItem);
    }

    cout << "Current State Items Size: " << st.items.size() << endl;

    for(int i = 0; i<st.items.size(); i++){
        Item it = st.items[i];
        string_vector_t rhs = it.prod.rhs;

        if(it.dot_pos == rhs.size()){
            cout << "\nNothing Left to Consume\n\n";
            continue;
        } 
        if(alreadyInTransitions(st.transitions, rhs[it.dot_pos])){
            cout << "\nTransition already added\n\n";
            continue;
        } 

        createState(st, it);
    }

    cout << "\nTRANSITION SYMBOLS\n\n";
    for(auto trans : st.transitions){
        cout << trans.symbol << endl;
    }

    cout << "Checking if already in DFA" << endl;
    cout << st.items[0].toString() << endl;
    if(!alreadyInDFA(st.items[0].toString())){
        cout << "not in it" << endl;
        dfa.push_back(st);
        cout << "After pushing to DFA" << endl;

        for(int i = 0; i<st.transitions.size(); i++){
            cout << "\nt" << i << "------------------" << endl;
            for(auto temp: st.transitions[i].next_state.items)
                cout << temp.toString() << endl;
        }

        for(int i = 0; i<st.transitions.size(); i++){
            genDFARecursive(st.transitions[i].next_state);
        }   
    }
}

vector<LR0::Item> LR0::getItems(State st, string symbol, bool lhs){
    vector<Item> its;
    cout << "Symbol: " << symbol << endl;
    if(lhs){
        for(auto & rule : gr.getRules())
            if(rule.lhs == symbol)
                its.emplace_back(rule, 0);
    }else{
        for(auto & item : st.items)
            if(item.prod.rhs[item.dot_pos] == symbol){
                cout << "Item prod for trans: " << item.toString() << endl;
                its.emplace_back(item.prod, item.dot_pos+1);
            }
    }

    return its;
}

bool LR0::isNonTerminal(string nonT){
    for(auto & nt : gr.getNonTerminals())
        if(nt == nonT) return true;
    return false;
}

bool LR0::alreadyInItems(vector<Item> items, Item i){
    for(auto it : items){
        if(it == i) return true;
    }
    return false;
}

bool LR0::alreadyInTransitions(vector<Transition> trans, string symbol){
    for(auto tran : trans)
        if(tran.symbol == symbol) return true;
    return false;
}

int LR0::findExistingState(string firstProd){
    for(int i = 0; i<dfa.size(); i++){
        State st = dfa[i];
        if(st.items[0].toString() == firstProd) return i;
    }
    return -1;
}

bool LR0::alreadyInDFA(string prod){
    for(auto state : dfa)
        if(state.items[0].toString() == prod) return true;
    return false;
}

void LR0::createState(State & currState, Item currItem){
    string_vector_t rhs = currItem.prod.rhs;
    vector<Item> itemsNewState = getItems(currState, rhs[currItem.dot_pos], false);

    cout << "Printing newItems of state with size: " << itemsNewState.size() << endl << endl;
    for(auto item : itemsNewState)
        cout << item.toString() << endl;
    cout << endl;

    int stateNumber = findExistingState(itemsNewState[0].toString());
    cout << "rhs[currItem.dot_pos]: " << rhs[currItem.dot_pos] << endl;

    if(stateNumber != -1){
        cout << "Found reusable state" << endl;
        currState.transitions.emplace_back(rhs[currItem.dot_pos], dfa[stateNumber]);
        return;
    }

    State * newState = new State();
    newState->clear();
    
    for(int i = 0; i<itemsNewState.size(); i++){
        cout << "i: " << i << " size: " << itemsNewState.size() << endl;
        newState->items.push_back(move(itemsNewState[i]));
    }

    cout << "NEXT STATE _--------------------------------\n\n";
    for(int i = 0; i<newState->items.size(); i++){
        cout << newState->items[i].toString() << endl;
        cout << newState->items[i].dot_pos << endl << endl;
    }

    currState.transitions.emplace_back(rhs[currItem.dot_pos], *newState);

    for(int i = 0; i<currState.transitions.size(); i++){
        cout << "\nq" << i << "------------------" << endl;
        cout << "Transition symbol: " << currState.transitions[i].symbol << endl;
        for(auto temp: currState.transitions[i].next_state.items){
            cout << temp.toString() << endl;
        }
    }
}