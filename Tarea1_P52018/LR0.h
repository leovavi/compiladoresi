#ifndef _LR0DFAGEN_H_
#define _LR0DFAGEN_H_

#include <vector>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <sstream>
#include "grammar.h"

using namespace std;

class LR0 {
public:
    using Production = Grammar::Production;

    struct Item {
        Item(const Production &prod, int dot_pos):
            prod(prod), dot_pos(dot_pos) {}

        bool operator==(const Item& rhs) {
            return (&prod == &(rhs.prod)) && (dot_pos == rhs.dot_pos);
        }

        bool operator!=(const Item& rhs) {
            return !(*this == rhs);
        }

        string toString(){
            string str = prod.lhs+" -> ";

            for(int i = 0; i<prod.rhs.size(); i++){
                if(i == dot_pos)
                    str += ". ";
                str += prod.rhs[i]+" ";
            }

            if(dot_pos == prod.rhs.size()) str += ".";

            return str;
        }

        const Production &prod;
        int dot_pos;
    };

    struct State;

    struct Transition {
        Transition(const std::string &symbol, State& next_state):
            symbol(symbol), next_state(next_state) {}

        std::string symbol;
        State& next_state;
    };

    struct State {
        bool operator==(const State& rhs) {
            if (items.size() != rhs.items.size()) {
                return false;
            }

            for (int i = 0; i < items.size(); i++) {
                if (items[i] != rhs.items[i]) {
                    return false;
                }
            }

            return true;
        }

        bool operator!=(const State& rhs) {
            return !(*this == rhs);
        }

        State() { clear(); };

        void clear(){
            items.clear();
            transitions.clear();
        }

        std::vector<Item> items;
        std::vector<Transition> transitions;
    };

    using DFA = std::vector<State>;

    LR0(Grammar &gr): gr(gr) {
        computeDFA();
    }
    const DFA& getDFA() { return dfa; }
    void printDFA();
    void genGraphviz();

private:
    void computeDFA();
    void genDFARecursive(State & st);
    vector<Item> getItems(State st, string symbol, bool lhs);
    bool isNonTerminal(string nonT);
    bool alreadyInItems(vector<Item> items, Item i);
    bool alreadyInTransitions(vector<Transition> trans, string symbol);
    bool alreadyInDFA(string prod);
    int findExistingState(string firstProd);
    void createState(State & currState, Item currItem);

private:
    Grammar &gr;
    DFA dfa;
    State ns;
};

#endif
