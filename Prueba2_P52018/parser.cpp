#include "parser.h"

void Parser::parse() {
    token = lexer.getNextToken();
    input();
    if(token == Token::Eof){
        std::cout << "Parsed successfuly!" << std::endl;
    }
    else{
        std::cout << "Unexpected end of parser, received: " << lexer.getText() << std::endl;
    }
}

void Parser::input()
{
    stmt_list();
    if(token == Token::Semicolon)
        token = lexer.getNextToken();
}

void Parser::stmt_list()
{
    stmt();
    if(token == Token::Semicolon){
        token = lexer.getNextToken();
        stmt_list_p();
    }
    else
        std::cout << "Expected semicolon but received: " << lexer.getText() << std::endl;
}

void Parser::stmt_list_p()
{
    stmt();
    if(token == Token::Semicolon){
        token = lexer.getNextToken();
        stmt_list_p();
    }
}

void Parser::stmt()
{
    if(token == Token::Ident){
        token = lexer.getNextToken();
        if(token == Token::OpAssign){
            token = lexer.getNextToken();
            expr();
        }
        else{
            std::cout << "Expected Operator = but received: " << lexer.getText() << std::endl;
        }
    }
    else if(token == Token::KwPrint){
        token = lexer.getNextToken();
        expr();
    }
    else if(token != Token::Eof)
        std::cout << "Expected IDENT or PRINT but received: " << lexer.getText() << std::endl;
}

void Parser::expr()
{
    term();
    expr_p();
}

void Parser::expr_p()
{
    if(token == Token::OpAdd){
        token = lexer.getNextToken();
        term();
        expr_p();
    }
}

void Parser::term()
{
    factor();
    term_p();
}

void Parser::term_p()
{
    if(token == Token::OpMult){
        token = lexer.getNextToken();
        factor();
        term_p();
    }
}

void Parser::factor()
{
    if(token == Token::Ident || token == Token::Number)
        token = lexer.getNextToken();
    else if(token == Token::OpenPar){
        token = lexer.getNextToken();
        expr();
        if(token == Token::ClosePar)
            token = lexer.getNextToken();
        else
            std::cout << "Expected ) but received: " << lexer.getText() << std::endl;
    }
    else
        std::cout << "Expected Number, Ident or ( but received: " << lexer.getText() << std::endl;
}