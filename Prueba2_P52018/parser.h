#ifndef _PARSER_H
#define _PARSER_H

#include "lexer.h"

class Parser {
public:
    Parser(Lexer& lexer): lexer(lexer) {}
    void parse();

private:
    Lexer& lexer;
    Token token;

    void input();
    void stmt_list();
    void stmt_list_p();
    void stmt();
    void expr();
    void expr_p();
    void term();
    void term_p();
    void factor();
};

#endif
