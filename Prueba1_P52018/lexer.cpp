#include "lexer.h"

Token Lexer::getNextToken() {
    currState = 0;
    text = "";

    while(true){
        switch(currState){
            case 0:
                if(ch == EOF) return Token::Eof; 
                if(ch == ' '){
                    ch = in.get();
                    continue;
                }
                if(ch == '0' || ch == '1')
                    addToText();
                else if(ch == 'b' || ch == 'B'){
                    addToText();
                    currState = 1;
                }
                else
                    currState = 2;
                break;
            case 1:
                return Token::Binary;
            case 2:
                if(ch >= '0' && ch <= '7')
                    addToText();
                else if(ch == 'o' || ch == 'O'){
                    addToText();
                    currState = 3;
                }
                else
                    currState = 4;
                break;
            case 3:
                return Token::Octal;
            case 4:
                if(ch >= '0' && ch <= '9')
                    addToText();
                else if(ch == ' ')
                    currState = 5;
                else
                    currState = 6;
                break;
            case 5:
                return Token::Decimal;
            case 6:
                if((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F'))
                    addToText();
                else if(ch == 'h' || ch == 'H'){
                    addToText();
                    currState = 7;
                }
                else 
                    currState = -1;
                break;
            case 7:
                return Token::Hex;
            default:
                return Token::Eof;
        }
    }
    return Token::Eof;
}

void Lexer::addToText()
{
    text += ch;
    ch = in.get();
}