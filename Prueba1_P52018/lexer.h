#ifndef _EXPR_LEXER_H
#define _EXPR_LEXER_H
#include <iostream>
#include <fstream>
#include <string>

enum class Token {
    Decimal,
    Hex,
    Binary,
    Octal,
    Eof
};

class Lexer {
public:
    Lexer(std::istream &in): in(in) {
        currState = 0;
        text = "";
        ch = in.get();
    }
    ~Lexer() {}

    Token getNextToken();
    std::string getText() { return text; }

private:
    std::istream &in;
    std::string text;
    int currState, ch;

    void addToText();
};
#endif
