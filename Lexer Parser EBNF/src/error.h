#ifndef _ERROR_H
#define _ERROR_H

#include <iostream>

using namespace std;

class Error{
    public:
        Error(string msg) { this->msg = msg; };
        string getErrorMessage() { return msg; };

    private:
        string msg;
};

#endif /* _ERROR_H */