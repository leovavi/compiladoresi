#include "sintax.h"

void Sintax::eval()
{
    int val = 0;
    tok = lexer.getNextToken();
    val = E();
    if(tok == Token::Eof){
        cout << "Parsed Successfully!" << endl;
        cout << "The value of the expression entered is: " << val << endl;
    }
    else
        throw Error("Unexpected end of Parser, received: "+lexer.getLexema());
}

int Sintax::E()
{
    int val = T();
    while(tok == Token::Plus){
        tok = lexer.getNextToken();
        val += T();
    }
    return val;
}

int Sintax::T()
{
    int val = F();
    while(tok == Token::Mult){
        tok = lexer.getNextToken();
        val *= F();
    }
    return val;
}

int Sintax::F()
{
    int val;
    if(tok == Token::OpenPar){
        tok = lexer.getNextToken();
        val = E();
        if(tok == Token::ClosePar)
            tok = lexer.getNextToken();
        else
            throw Error("Expected ) but received: "+lexer.getLexema());    
    }
    else if(tok == Token::Num){
        cout << lexer.getLexema() << endl;
        val = atoi(lexer.getLexema().c_str());
        cout << "Val: " << val << endl;
        tok = lexer.getNextToken();
    }
    else 
        throw Error("Expected ( or Number but received: "+lexer.getLexema());
    return val;
}