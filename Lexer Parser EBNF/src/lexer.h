#ifndef _NUMLEXER_H
#define _NUMLEXER_H

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <functional>
#include <cstdio>
#include <cctype>
#include <cstdlib>

using namespace std;

enum class Token {Num, Plus, Mult, OpenPar, ClosePar, Undefined, Eof};

class Lexer{
    public:
        Lexer(ifstream &file) : file(file){
            lexema = "";
            getNextChar();
        };

        Token getNextToken();
        string TokenToString(Token tok);
        string getLexema(){ return lexema; };

    private:
        ifstream &file;
        string lexema;
        int ch;

        void addToLexema();
        void getNextChar(){ ch = file.get(); };
};

#endif /* _NUMLEXER_H */
