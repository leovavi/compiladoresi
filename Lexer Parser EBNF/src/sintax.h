#ifndef _SINTAX_H
#define _SINTAX_H

#include "lexer.h"
#include "error.h"

class Sintax{
    public:
        Sintax(Lexer &lex) : lexer(lex) {};
        void eval();
    private:
        Lexer & lexer;
        Token tok;

        int E();
        int T();
        int F();

};

#endif /* _SINTAX_H */