#include "sintax.h"

int main(int argc, char * argv[])
{
    if(argc != 2){
        cout << "Usage " << argv[0] << " <File Name>" << endl;
        return 1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "Could't open file: " << argv[1] << endl;
        return 2;
    }

    Lexer lex(file);
    Sintax sin(lex);
    
    /*Token tok = lex.getNextToken();

    while(tok != Token::Eof){
        cout << "Token: " << lex.TokenToString(tok) << " - Lexema: " << lex.getLexema() << endl;
        tok = lex.getNextToken();
    }*/

    try{
        sin.eval();
    }catch(Error e){
        cout << "Error while parsing: " << e.getErrorMessage() << endl;
    }

    return 0;
}