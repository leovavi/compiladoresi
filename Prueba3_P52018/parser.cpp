#include "parser.h"

void Parser::parse() {
    token = lexer.getNextToken();
    Token last;

    while(true){
        last = stack.at(stack.size()-1);
        stack.pop_back();

        if(last == Token::Eof && token == Token::Eof)
            break;

        if(isTerminal(last) && last == token){
            token = lexer.getNextToken();
            continue;
        }

        Prod * p = getProduction(last, token);
        if(p == nullptr)
            throw "Error while parsing";
        else if(p == NULL)
            throw "Unexpected non terminal";
        
        for(int i = p->cant-1; i>=0; i--)
            stack.push_back(p->toks[i]);
    }
    
    std::cout << "Parsed successfuly!" << std::endl;
}

Prod * Parser::getProduction(Token nonT, Token T){
    switch(nonT){
        case Token::Input:
            switch(T){ 
                case Token::Ident: 
                case Token::KwPrint:
                case Token::Eof: return &input_prod;
                default: return nullptr;
            }
        case Token::Stmt_list:
            switch(T){
                case Token::Ident:
                case Token::KwPrint: return &stmt_list_prod1;
                case Token::Eof: return &stmt_list_prod2;
                default: return nullptr;
            }
        case Token::Stmt:
            switch(T){
                case Token::Ident: return &stmt_prod1;
                case Token::KwPrint: return &stmt_prod2;
                default: return nullptr;
            }
        case Token::Expr:
            switch(T){
                case Token::Ident:
                case Token::Number:
                case Token::OpenPar: return &expr_prod;
                default: return nullptr;
            }
        case Token::Expr_p:
            switch(T){
                case Token::OpAdd: return &expr_p_prod1;
                case Token::Semicolon:
                case Token::ClosePar: return &expr_p_prod2;
                default: return nullptr;
            }
        case Token::Term:
            switch(T){
                case Token::Ident:
                case Token::Number:
                case Token::OpenPar: return &term_prod;
                default: return nullptr;
            }
        case Token::Term_p:
            switch(T){
                case Token::OpMult: return &term_p_prod1;
                case Token::Semicolon:
                case Token::OpAdd:
                case Token::ClosePar: return &term_p_prod2;
                default: return nullptr;
            }
        case Token::Factor:
            switch(T){
                case Token::Ident: return &factor_prod1;
                case Token::Number: return &factor_prod2;
                case Token::OpenPar: return &factor_prod3;
                default: return nullptr;
            }
        default: return NULL;
    }
}

std::string Parser::TokenTOS(Token t){
    switch(t){
        case Token::Input: return "Input";
        case Token::Stmt_list: return "Stmt_List";
        case Token::Stmt: return "Stmt";
        case Token::Expr: return "Expr";
        case Token::Term: return "Term";
        case Token::Factor: return "Factor";
        default: return "Terminal";
    }
}
