#ifndef _PARSER_H
#define _PARSER_H

#include "lexer.h"
#include <vector>

typedef struct Prod{
    int cant;
    Token * toks;
} Prod;

class Parser {
public:
    Parser(Lexer& lexer): lexer(lexer) {
        stack.push_back(Token::Eof);
        stack.push_back(Token::Input);
    }
    void parse();

private:
    std::vector<Token> stack;
    Lexer& lexer;
    Token token;

    Token input_toks[1] = {Token::Stmt_list};
    Token stmt_list_toks[3] = {Token::Stmt, Token::Semicolon, Token::Stmt_list};
    Token stmt_toks1[3] = {Token::Ident, Token::OpAssign, Token::Expr};
    Token stmt_toks2[2] = {Token::KwPrint, Token::Expr};
    Token expr_toks[2] = {Token::Term, Token::Expr_p};
    Token expr_p_toks[3] = {Token::OpAdd, Token::Term, Token::Expr_p};
    Token term_toks[2] = {Token::Factor, Token::Term_p};
    Token term_p_toks[3] = {Token::OpMult, Token::Factor, Token::Term_p};
    Token factor_toks1[1] = {Token::Ident};
    Token factor_toks2[1] = {Token::Number};
    Token factor_toks3[3] = {Token::OpenPar, Token::Expr, Token::ClosePar};

    Prod input_prod = {.cant=1, .toks=input_toks};
    Prod stmt_list_prod1 = {.cant=3, .toks=stmt_list_toks};
    Prod stmt_list_prod2 = {.cant=0, .toks=nullptr};
    Prod stmt_prod1 = {.cant=3, .toks=stmt_toks1};
    Prod stmt_prod2 = {.cant=2, .toks=stmt_toks2};
    Prod expr_prod = {.cant=2, .toks=expr_toks};
    Prod expr_p_prod1 = {.cant=3, .toks=expr_p_toks};
    Prod expr_p_prod2 = {.cant=0, .toks=nullptr};
    Prod term_prod = {.cant=2, .toks=term_toks};
    Prod term_p_prod1 = {.cant=3, .toks=term_p_toks};
    Prod term_p_prod2 = {.cant=0, .toks=nullptr};
    Prod factor_prod1 = {.cant=1, .toks=factor_toks1};
    Prod factor_prod2 = {.cant=1, .toks=factor_toks2};
    Prod factor_prod3 = {.cant=3, .toks=factor_toks3};

    bool isTerminal(Token t) {
        return static_cast<unsigned int>(t) <= 255;
    }

    Prod * getProduction(Token nonT, Token T);
    std::string TokenTOS(Token t);
};

#endif
